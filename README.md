Fork from https://github.com/dwm9100b/lsWires.git by Dale Morrison

# lsWires

### Wiring up Visual Studio LightSwitch with useful stuff!

lsWires is a work in progress with new functionality being added all the time.  In fact we are starting a consolidation effort with all of our projects into this unified library.  As with any refactoring/consolidation efforts, this may break some current implementations .  

We are starting out with the following:

**Enhancements to button controls/elements**

```
#!javascript

lsWire.button.changeIcon
	Change the icon of an existing iconic button
lsWire.button.renderAsIcon
	Render a standard button as an iconic button

```

**Enhancements to input controls/elements**

```
#!javascript

lsWire.input.characterLimit
	Add character limits and realtime character count on the label
lsWire.input.updateCharacterCountLabel
	Update/refresh the character count on a label
```

**Enhancements to list/table controls**

```
#!javascript

<s>lsWire.list.itemTap</s>
<s>lsWire.list.unselectAll</s>
lsWire.list.enableMultiSelect
	Enable multi-item selection on a list or table
lsWire.list.reselect
	Reselect a previously selected list or table item
lsWire.list.selected
	Get an array of entities from the selected items
lsWire.list.selectAll
	Select/Unselect all items in a list or table
lsWire.list.selectedCount
	Get a count of the number of selected items
lsWire.list.totalSelectionsAllowed
	Get/Set the total number of selections allowed
lsWire.list.persistSelections
	Get/Set a flag allowing session persistent selections
```


**Providing checkbox functionality**

```
#!javascript

lsWire.checkbox.render
	Render a boolean custom control as a checkbox<
lsWire.checkbox.updateTextCssClasses
	Update CSS class for the checkbox text
lsWire.checkbox.setText
	Set the text label of a checkbox
lsWire.checkbox.addCssClassForText
	Add a CSS class for the text label
lsWire.checkbox.removeCssClassForText
	Remove a CSS class for the text label
lsWire.checkbox.initializeCss
	Pre-initializes the CSS for a checkbox
```

**Enhancements to Layout controls**

```
#!javascript

lsWire.layout.renderHeader
	Provides a way to render a header for a Plain Ol Layout control (POL)
```


**Utility Helpers**

```
#!javascript

lsWire.utility.findParentByTagName
	Get the parent of an element that matches a tag
lsWire.utility.findParentByClassName
	Get the parent of an element that matchs a CSS Class
lsWire.utility.createProperty
	Create a LS property programmatically
```

The code is heavily commented along with full intellisense.  We also have tutorials on the blog to help with learning how to implement these enhancements.

Until the code is stable and feature complete, we will avoid putting it into NuGet.  Unless folks think otherwise and see the value over the risks.